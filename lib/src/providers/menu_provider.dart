//show = solo traeme rootBundle del paquete services.dart
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class _MenuProvider {
  List<dynamic> opciones = [];

  _MenuProvider() {
    //cargarData();
  }

  Future<List<dynamic>> cargarData() async {
    //loadString = para cargar los archivos y obtenerlos en formato String
    final resp = await rootBundle.loadString('data/menu_opts.json');

    //Convierte la variable String en un Json object
    Map dataMap = json.decode( resp );
    opciones = dataMap['rutas'];

    return opciones;
  }
}

final menuProvider = new _MenuProvider();