import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {

  /** Una manera de poner el valor para el route */
  static final pageName = 'avatar';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: [
          Container(
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://clarovideocdn5.clarovideo.net/CRACKLE/PELICULAS/100001898/EXPORTACION_WEB/SS/100001898WHORIZONTAL.jpg?size=675x380'),
              radius: 30.0,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('SL'),
              backgroundColor: Colors.brown,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://clarovideocdn5.clarovideo.net/CRACKLE/PELICULAS/100001898/EXPORTACION_WEB/SS/100001898WHORIZONTAL.jpg?size=675x380'),
          placeholder: AssetImage('assets/jar-loading.gif'),
          fadeInDuration: Duration(milliseconds: 200),
        ),
      ),
    );
  }
}