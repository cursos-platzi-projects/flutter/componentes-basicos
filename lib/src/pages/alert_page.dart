import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Page'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Mostrar alerta'),
          onPressed: () => _mostrarAlert(context),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            shape: StadiumBorder()
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon( Icons.add_location ),
        onPressed: () {
          Navigator.pop(context); // para regresar a la anterior pagina
        },
      ),
    );
  }


  void _mostrarAlert(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true, //Para que se pueda cerrar la modal desde afuera,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: Text('Titulo'),
          content: Column(
            mainAxisSize: MainAxisSize.min, //Se adapta el size al contenido que tenga ahorita, como un auto
            children: [
              Text('Este es el contenido de la caja de la alerta'),
              FlutterLogo(size: 100,)
            ],
          ),
          actions: [
            TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Cancelar')
            ),
            TextButton(
                onPressed: () {},
                child: Text('Ok')
            ),
          ],
        );
      }
    );
  }
}