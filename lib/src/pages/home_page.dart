import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista()
    );
  }

  Widget _lista() {

    //Esta alternativa no es conveniente por si acaso hay un error y no se quede la app cargando por los datos
    /*menuProvider.cargarData().then( (opciones) {
      print( '_lista' );
      print(opciones);
    });

    return ListView(
      children: _listaItems(),
    );

    */

    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [], // es opcional
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot ) {
        /**
         * Se ejecuta cuando:
         * 1- Estamos pidiendo la data
         * 2- Cuando haya cargado la data
         * 3- Cuando hay un error
         */

        return ListView(
          /** ??[] = Esto significa para evitar valores nulos*/
          children: _listaItems(snapshot.data??[], context),
        );


      },
    );


  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {

    final List<Widget> opciones = [];


    data.forEach(( opt ) {

      final widgetTemp = ListTile(
        title: Text( opt['texto'] ),
        leading: getIcon(opt['icon']),
        trailing: Icon( Icons.keyboard_arrow_right, color: Colors.blue ),
        onTap: () {

          Navigator.pushNamed(context, opt['ruta']);

          /** Ejemplo sencillo de navegacion */
          /*//Mas larga
          final route = MaterialPageRoute(
            builder: ( context ) {
              return AlertPage();
            }
          );

          //Mas corta
          final route = MaterialPageRoute(
              builder: ( context ) => AlertPage()
          );

          Navigator.push(context, route);*/
        },
      );

      opciones..add( widgetTemp )
              ..add( Divider() );

    });

    return opciones;


    /** Un ejemplo sin usar el foreach */
    /*return const [
      ListTile(
        title: Text('Hola mundo'),
      ),
      Divider(),
      ListTile(
        title: Text('Hola mundo'),
      ),
      Divider(),
      ListTile(
        title: Text('Hola mundo'),
      ),
    ];*/
  }
}