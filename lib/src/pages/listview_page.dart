import 'dart:async';

import 'package:flutter/material.dart';

class ListaPage extends StatefulWidget {
  const ListaPage({Key? key}) : super(key: key);

  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<ListaPage> {

  ScrollController _scrollController = new ScrollController();

  List<int> _listaNumeros = [];
  int _ultimoNumero = 0;
  bool _isLoading = false;

  /** Metodo que asi deberia llamarse "initState" y no regresa algun valor */
  @override
  void initState() {
    /** Esto hace referencia a la clase State */
    super.initState();
    _agregar10();


    /** Esto se dispara cada vez que el scroll se mueva */
    _scrollController.addListener(() {

      /** Condicion para saber si se llego hasta el final de la pantalla */
      if( _scrollController.position.pixels == _scrollController.position.maxScrollExtent ) {
        //_agregar10();
        fetchData();
      }

    });
  }

  /** Se dispara cuando la pagina deja de existir o nos hayamos salido de ella */
  @override
  void dispose() {
    super.dispose();
    /** Esto para evitar fugas de memoria */
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listas'),
      ),
      body: Stack( //El Stack apila unos con otros, esto es parecido al (Column, Row)
        children: [
          _crearLista(),
          _crearLoading(),
        ],
      )
    );
  }

  Widget _crearLista() {
    return RefreshIndicator(
      onRefresh: () => obtenerPagina1(),
      child: ListView.builder(
        controller: _scrollController,
        itemCount: _listaNumeros.length,
        itemBuilder: ( BuildContext context, int index ) {

          /**Esto era para poner en la URL pero actualmente no funciona*/
          final imagen = _listaNumeros[index];

          return FadeInImage(
            image: NetworkImage('https://picsum.photos/seed/picsum/500/300'),
            placeholder: AssetImage('assets/jar-loading.gif'),
          );
        },
      ),
    );
  }

  Future<void> obtenerPagina1() async {
    final duration = new Duration(seconds: 2);
    new Timer(duration, () {

      _listaNumeros.clear();

      /** Aqui deberia setear a 0 pero como se desea ver imagenes nuevas pues se pone como sumando valores
       * Se recuerda que esto no funciona ya que no hay un index en la imagen
       */
      _ultimoNumero++;
      _agregar10();

    });

    return Future.delayed(duration);
  }

  void _agregar10() {
    for(var i = 1; i < 10; i++) {
      _ultimoNumero++;
      _listaNumeros.add( _ultimoNumero );
    }

    setState(() {

    });
  }

  Future fetchData() async {

    _isLoading = true;
    setState(() {}); //Mardar a llamarla para que el Widget tenga que cargarlo en automatico

    /** Simula un retraso de 2 segundos para poder mostrar el loading */
    final duration = new Duration(seconds: 2);
    return new Timer(duration, respuestaHTTP);
  }

  void respuestaHTTP() {
    _isLoading = false;


    /** La animacion es para cuando termine de cargar el fech, haga un scroll pequeño hacia abajo para
     * mostrar que hay mas resultados por ver */
    _scrollController.animateTo(
      _scrollController.position.pixels + 100,
      curve: Curves.fastOutSlowIn,
      duration: Duration(milliseconds: 250)
    );

    _agregar10();
  }

  Widget _crearLoading() {
    if( _isLoading ) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
              ],
            ),
          SizedBox(height: 15.0)
        ],
      );

    } else {
      return Container();
    }
  }
}
