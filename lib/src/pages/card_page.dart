import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards'),
      ),
      body: ListView(
        //padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
        padding: EdgeInsets.all(10.0), // Para horizontal y vertical
        children: [
          _cardTipo1(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),
          _cardTipo2(),
          SizedBox(height: 30.0,),

        ],
      ),
    );
  }

  Widget _cardTipo1() {
    return Card(
      // Para el efecto de sale abajo del Card
      elevation: 5.0,
      // Para darle un border radius al Card
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)  ),
      child: Column(
        children: [
          const ListTile(
            leading: Icon( Icons.photo_album, color: Colors.blue, ),
            title: Text('Este es mi titulo de la tarjeta'),
            subtitle: Text('Solo estamos haciendo el curso de flutter con este subtitulo para ver como se ve esto'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end, // Para aliniar los botones a la derecha
           children: [
             TextButton(
               child: const Text('Cancelar'),
               onPressed: () {},
             ),
             TextButton(
               child: const Text('Ok'),
               onPressed: () {},
             )
           ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2() {
    final card = Container(
      //clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          const FadeInImage(
            image: NetworkImage('https://cdn3.dpmag.com/2021/07/Landscape-Tips-Mike-Mezeul-II.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration( milliseconds: 200 ), //Es opcional para poner la duracion
            height: 300.0,
            fit: BoxFit.cover, // Esto es para que se adapte la imagen a todo el container
          ),
          /*Image(
            image: NetworkImage('https://cdn3.dpmag.com/2021/07/Landscape-Tips-Mike-Mezeul-II.jpg'),
          ),*/
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('No tengo idea de que poner'),
          )
        ],
      ),
    );

    //Container = Esto es como un div
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0, //Para expandir el border radius
            offset: Offset(2.0, 10.0)
          )
        ]
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }
}