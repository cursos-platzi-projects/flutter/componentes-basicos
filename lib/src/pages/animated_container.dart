import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

/**
 * Resources:
 * https://api.flutter.dev/flutter/animation/Curves-class.html - Curves
 * */
class AnimatedContainerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AnimatedContainerPageState();

}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {
  
  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.pink;
  
  BorderRadiusGeometry _borderRadius = BorderRadius.circular(8.0);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Center(
        //El Container paso a ser AnimatedContainer
        child: AnimatedContainer(
          duration: Duration(milliseconds: 200),
          curve: Curves.fastOutSlowIn, //Para darle un efecto
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            borderRadius: _borderRadius,
            color: _color
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_circle_outlined),
        onPressed: () => _cambiarForma(),
      ),
    );
  }

  void _cambiarForma() {

    final random = Random();

    setState(() {

      _width = random.nextInt(300).toDouble();
      _height = random.nextInt(300).toDouble();

      //Para darle un color diferente cuando cambie
      _color = Color.fromRGBO(
          random.nextInt(255),
          random.nextInt(255),
          random.nextInt(255),
          1);
      
      _borderRadius = BorderRadius.circular(random.nextInt(300).toDouble());
    });
  }
}